#!/bin/bash

set -ex

export GIT_AUTHOR_NAME="CMS Submission Infrastructure"
export GIT_AUTHOR_EMAIL="cms-service-glideinwms-admins@cern.ch"
export GIT_COMMITTER_NAME="$GIT_AUTHOR_NAME"
export GIT_COMMITTER_EMAIL="$GIT_AUTHOR_EMAIL"

git remote set-url origin "https://actions:${ACCESS_TOKEN}@gitlab.cern.ch/cmssi/cmsgwms-frontend-configurations.git"

if !(git diff --exit-code); then
    commitish="$(git describe --always)"
    git add -A
    git checkout "$CI_COMMIT_BRANCH"
    git commit -m "Update with changes from $commitish"
    git push -u origin "$CI_COMMIT_BRANCH"
fi

do_pr() {
    MR_SOURCE_BRANCH="$1"
    MR_TARGET_BRANCH="$2"
    MR_TITLE="Updated configurations for the $MR_TARGET_BRANCH frontend"
    count_mrs="$(curl --header "PRIVATE-TOKEN: $ACCESS_TOKEN" "https://gitlab.cern.ch/api/v4/projects/11496/merge_requests?state=opened&target_branch=$MR_TARGET_BRANCH" | jq length)"
    if [[ "$count_mrs" == 1 ]]; then
        exit 0
    elif [[ "$count_mrs" > 1 ]]; then
        echo "Invalid number of pull requests found!"
        exit 1
    fi

    curl -X POST --header "PRIVATE-TOKEN: $ACCESS_TOKEN" -H "Content-Type: application/json"  -d "{\"source_branch\":\"$MR_SOURCE_BRANCH\",\"target_branch\":\"$MR_TARGET_BRANCH\",\"title\":\"$MR_TITLE\"}" https://gitlab.cern.ch/api/v4/projects/11496/merge_requests
}

if ! (git diff --quiet "origin/$CI_COMMIT_BRANCH..origin/cern" -- compiled/cern); then
    do_pr "$CI_COMMIT_BRANCH" "cern"
fi

if ! (git diff --quiet "origin/$CI_COMMIT_BRANCH..origin/fnal" -- compiled/fnal); then
    do_pr "$CI_COMMIT_BRANCH" "fnal"
fi
